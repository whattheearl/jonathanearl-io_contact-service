

// endpoint https://us-central1-jonathanearlio.cloudfunctions.net/contact-service

const nodemailer = require('nodemailer')
const validator = require('validator')
const cors = require('cors')({origin: true})
require('dotenv').config()

console.info(process.env.EMAIL)

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: process.env.GM_USER,
           pass: process.env.GM_PASS
       }
   });

exports.email = (req, res) => {
    // allow ajax requests
    cors(req, res, () => {
        // verify user input
        if(!isUserDataValid(req.body)) {
            return res.status(403).json({error: true, msg: "Please enter a valid Email! (╯°□°）╯︵ ┻━┻"})
        }
        // send info to myself
        sendClientInfoToEarl(req.body)
        .then(() => {
            // send success to user
            sendSuccessEmailToClient(req.body)
            .then(() => {
                return res.status(200).json({error: false, msg: "Talk to you soon!"})
            })
            // error sending success
            .catch(err => {
                sendErrorToEarl(req.body, err)
                console.info('err', err)
                return res.status(500).json({error: true, msg: "Something went wrong! Please try again later! (╯°□°）╯︵ ┻━┻"})
            })
        })
        // error sending info to self
        .catch(err => {
            sendErrorToEarl(req.body, err)
            console.info('err', err)
            return res.status(500).json({error: true, msg: "Something went wrong! Please try again later! (╯°□°）╯︵ ┻━┻"})
        })
    })
}

function isUserDataValid(data) {
    let {isEmpty, isEmail} = validator
    let {name, email, msg} = data
    
    if(name === undefined || email === undefined || msg === undefined) {
        console.info(`${name}, ${email}, ${msg}: failed undefined check`)
        return false
    }
    try {
        // ensure data is sent
        if (isEmpty(name) || isEmpty(email) || isEmpty(msg)) {
            console.info(`${name}, ${email}, ${msg}: failed empty check`)
            return false
        }
        if(!isEmail(email)) {
            console.info(`${email}: failed email check`)
            return false
        }
        // ensure reasonable name/msg lengths, previous functions ensure these are strings
        if(msg.length > 300 || name.length > 100) {
            console.info(`${name}, ${msg}: failed empty check`)
            return false
        }
    } catch(e) {
        console.info(e)
        return false
    }
    return true
}

function sendSuccessEmailToClient(body) {
    let mailOptions = {
        to: body.email,
        subject: 'Jonathan Earl - Message Recieved',
        html: 
            `<h1>Hello ${body.name}!</h1>
            <p>Thank you for the message, I look forward to talking to you!</p>`
    }
    return transporter.sendMail(mailOptions)
}

function sendClientInfoToEarl(body) {
    let mailOptions = {
        to: process.env.EMAIL,
        subject: `New Contact - ${body.name} - ${body.email}`,
        html: 
            `<h1>Info</h1>
            <bold>Name:</bold> ${body.name} <br/>
            <bold>Email:</bold> ${body.email} <br/>
            <bold>Message:</bold> ${body.msg} <br/>`
    }
    return transporter.sendMail(mailOptions)
}

function sendErrorToEarl(body, err) {
    let mailOptions = {
        to: process.env.EMAIL,
        subject: `New Contact - ${body.name} - ${body.email}`,
        html: 
            `<h1>Info</h1>
            <bold>Name:</bold> ${body.name} <br/>
            <bold>Email:</bold> ${body.email} <br/>
            <bold>Message:</bold> ${body.msg} <br/>
            <bold>Error:</bold> ${err}`
    }
    return transporter.sendMail(mailOptions)
}